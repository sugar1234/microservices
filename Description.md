### Proposal

We propose a central location to offer microservices based on the kin ecosystem. The price will be subject to the free market. Anyone can offer any service - barring illegal ones - for any price. It is envisioned to become a hyperlocal community center. 

###  Features
* Rating system
* KYC
* Community approved services for minors.
* Escrow
* Open source
* It will function like a traditional market with bids and asks.
    
### Use Cases:
* Mow the Lawn
* Shovel Snow
* Accept parcels for your neighbour.
    * This could help Amazon and co. -DHL, Fedex, etc- save millions in lost revenue on account of fraud and misdeliveries. To really be effective it would have to be integrated in their respective apps though to take advantage of the network effect.
* Share your storage space.
* Community Gym that uses a very cheap pay-per-visit model.
* Share your wifi with your neighbour or your visiting friend.
* Share your famous muffins with the neighbours kids.
* Computer repair for  the elderly in the community.
* Moving assistance
* Pet Care

### Stories:
* Sam, 23 years old.
    * It's the end of the month and you are short on money. You are awaiting your paycheck any day now. A couple of good friends have come from out of town and want to party at that new club that just opened. What are your options? 
    * Man, what can i do.. I really want to go but there is no option for me. Unless.. wait! I just saw a poster today on the local bakery for a new app. Let me try this.
    * - a couple of minutes later - OMG this is awesome. Ok, i have to bake 30 of my famous muffins, put up a -Instagram worthy- delicious photo up and ping some local kids touring around on their bicycles to deliver them to anyone in the community that has an open bid for muffins! Why am i talking to myself in perfect grammar? Anyway.. Let's DO THIS!!

* Taylor, 72 years old.
    * Your hip pain is flaring up today. Your partner is visiting their nephew for the week. Your dogs need walking but there is no way you can do that alone.
    * My cute little babies. I'm so sorry. I can't take you for a walk today :( 
    * - a short nap later - Eureka! I will use the app my grandson showed me. I will have to find my phone first. Where could it be..
    * - some undefined time in the future - Ok i think i got this down now. I will put up a standing offer - only for local kids - that can trigger a maximum of twice a day for the next week. MY BABIES, i saved us all! I knew it would pay off to take the time to learn and use this phone. Why do they call them smartphones if i have to do all the work?!
 

### Technical:
* A crossplatform framework will be used to build the same app for both Android and iOS. Something like React Native.
* 


